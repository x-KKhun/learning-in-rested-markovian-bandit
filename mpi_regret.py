# -*- coding: utf-8 -*-
__author__ = "Kimang Khun"
__email__ = "khun.kimang@gmail.com"

""" main script for the experiment

We can perform all algorithms or one algorithm individually

"""

from __future__ import division, print_function # Python 2 compatibility



import numpy as np
import sys
from utils.experiments import simulate_agent, compute_regret, make_RandomWalk, make_TaskScheduling, compute_bayesian_regret
from utils.MDP_convertor import make_global_MDP
from learners.mb_psrl import MB_PSRL
from learners.psrl import PSRL
from learners.ucrl2 import UCRL2
from learners.mb_ucbvi import MB_UCBVI

from mpi4py import MPI
comm = MPI.COMM_WORLD
nb_procs = comm.Get_size()
proc_id = comm.Get_rank()

#Parameters
discount_factor = float(sys.argv[1])
nb_episodes = int(sys.argv[2])
nb_simulations = int(sys.argv[3])
task_scheduling = False
prior_choice_experiment = False
delta = 1.0/nb_episodes

if int(sys.argv[4]) == 0:   # case 3bandits 4states
    if proc_id == 0:        # root processor
        print("Test on Random Walk")
    N_bandits = 3
    nb_states = 4
    P_1, R_1 = make_RandomWalk(nb_states, 0.2, 1.0, 0.1, 0.2, 0.3)
    P_2, R_2 = make_RandomWalk(nb_states, 0.35, 0.7, 0.1, 0.5, 0.7)
    P_3, R_3 = make_RandomWalk(nb_states, 0.4, 0.65, 0.1, 0.4, 0.5)

    Ps = [P_1, P_2, P_3]
    Rs = [R_1, R_2, R_3]
    agent_list = [
            MB_PSRL(N_bandits, nb_states, discount_factor, task_scheduling), # around 28seconds/simulation(beta=0.99, 3000 episdes)
            MB_UCBVI(N_bandits, nb_states, discount_factor, delta, task_scheduling),#, # around 28seconds/simulation(beta=0.99, 3000 episdes)
            UCRL2(N_bandits, nb_states, discount_factor, delta, task_scheduling, constraint=True)]#, # MB-UCRL2 around 50 mins/simulation(beta=0.99, 3000 episdes)

elif int(sys.argv[4]) == 2: # bayesian regret: prior choice test
    nb_mdps_per_proc = int(sys.argv[5]) #number of mdps to be drawn by each processor
    prior_choice_experiment = True
    N_bandits = 3
    nb_states = 4
    if proc_id == 0:
        print("Correct Reward distribution: Bernoulli")
        import h5py
        fname = "./data/prior_choice.hdf5"
        f = h5py.File(fname, "w")
        f.attrs['discount factor'] = discount_factor
        f.attrs['nb mdps'] = nb_procs*nb_mdps_per_proc
        f.attrs['nb sims per mdp'] = nb_simulations
        f.attrs['nb_episodes'] = nb_episodes
        grp_MB_PSRL = f.create_group("MB-PSRL")
        grp_PSRL = f.create_group("PSRL")

    agent_list = [
            MB_PSRL(N_bandits, nb_states, discount_factor, task_scheduling), # around 1min11seconds/simulation(beta=0.99, 3000 episodes) Beta+Gaussian
            PSRL(N_bandits, nb_states, discount_factor, task_scheduling, constraint=False)] # around 3mins/simulation(beta=0.99, 3000 episodes) Beta+Gaussian
    # by default, Bayesian algorithms have Beta prior

    for agent in agent_list:
        print("Test", agent.name(), "with Beta prior")
        results = compute_bayesian_regret(comm, nb_procs, proc_id, nb_simulations, nb_episodes, nb_mdps_per_proc, agent)
        if proc_id == 0:
            recv_gaps = results[0]
            if agent.name() == "MB-PSRL":
                grp_MB_PSRL.create_dataset("Correct Prior", (nb_simulations*nb_mdps_per_proc*nb_procs, nb_episodes), dtype='float', data=recv_gaps)
            elif agent.name() == "PSRL":
                grp_PSRL.create_dataset("Correct Prior", (nb_simulations*nb_mdps_per_proc*nb_procs, nb_episodes), dtype='float', data=recv_gaps)
        del results #clean up

    for agent in agent_list:
        print("Test", agent.name(), "with Gaussian-Gamma prior")
        agent.pu_idx = 1 # change prior to Gaussian-Gamma
        agent.reset()
        results = compute_bayesian_regret(comm, nb_procs, proc_id, nb_simulations, nb_episodes, nb_mdps_per_proc, agent)
        if proc_id == 0:
            recv_gaps = results[0]
            if agent.name() == "MB-PSRL":
                grp_MB_PSRL.create_dataset("Incorrect Prior", (nb_simulations*nb_mdps_per_proc*nb_procs, nb_episodes), dtype='float', data=recv_gaps)
            elif agent.name() == "PSRL":
                grp_PSRL.create_dataset("Incorrect Prior", (nb_simulations*nb_mdps_per_proc*nb_procs, nb_episodes), dtype='float', data=recv_gaps)
        del agent, results #clean up

    if proc_id == 0:
        f.close()
        print("Successfully finish! The result is stored in {}".format(fname))

elif int(sys.argv[4]) == 1: # task scheduling
    if proc_id == 0:        # root processor
        print("Test on Task Scheduling")
    N_bandits = 9
    nb_states = 11
    lamb = 0.8              # lambda: parameter of the example
    ### !BECAREFUL: lamb -> 1 or lamb fixed but nb_states -> infty can cause error
    Ps, Rs = make_TaskScheduling(nb_states, N_bandits, lamb)
    task_scheduling = True
    agent_list = [
            #MB_PSRL(N_bandits, nb_states, discount_factor, task_scheduling), #50 seconds/simulation(beta=0.99, 3000 episodes)
            MB_UCBVI(N_bandits, nb_states, discount_factor, delta, task_scheduling)]
            #Gittins_QLearning(N_bandits, nb_states, discount_factor, task_scheduling)] #50 seconds/simulation(beta=0.99, 3000 episodes)
else:
    raise IOError("Unknown Scenario, line 16, in launchWithNix.sh.")

if not prior_choice_experiment:
    if proc_id == 0:
        import h5py
        fname = "./data/result_{}b{}s.hdf5".format(N_bandits, nb_states)
        f = h5py.File(fname, "w")
        f.attrs['discount_factor'] = discount_factor
        f.attrs['nb_episodes'] = nb_episodes
        f.attrs['nb_simulations'] = nb_simulations*nb_procs
        regret_grp = f.create_group("regret gaps")
        time_grp = f.create_group("computation time")

    for agent in agent_list:

        print(agent.name())
        results = compute_regret(comm, nb_procs, proc_id, nb_simulations, nb_episodes, Ps, Rs, agent)

        if proc_id == 0:
            recv_gaps = results[0]
            recv_times = results[1]
            regret_grp.create_dataset(agent.name(), (nb_simulations*nb_procs, nb_episodes), dtype='float', data=recv_gaps)
            time_grp.create_dataset(agent.name(), (nb_simulations*nb_procs, nb_episodes), dtype='float', data=recv_times)
            del recv_gaps, recv_times #clean up
        del agent, results #clean up

    if proc_id == 0:
        f.close()
        print("Successfully finish! The result is stored in {}".format(fname))
