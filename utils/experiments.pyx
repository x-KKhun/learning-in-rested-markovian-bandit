# -*- coding: utf-8 -*-

""" List of functions

- argmax: tie-breaking rule when several states have maximal index
- compute_GittinsIdx: compute Gitins indices of a bandit
- make_RandomWalk: create transition matrix and reward vector for Random Walk scenario
- make_TaskScheduling: create transition matrices and reward vectors for Task Scheduling scenario
- calculate_exact_value: compute exact value of a given policy
- simulate_agent: rollout the algorithm over a given number of episodes and return regret gap for each episode
- compute_regret: call simulate_agent for a given number of simulations and gather all regret gaps of each simulation
- compute_bayesian_regret: draw mdps and create models than call simulate_agent for a given number of simulations and gather all regret gaps of each simulation on root processor. Each processor can draw only one mdp, so more processors, more mdps drawn
- simulate_agent_ts: rollout the algorithm over a given number of episodes and return all reward gap of each time step
- compute_regret_ts: call simulate_agent_ts for a given number of simulations and gather all reward gaps of each simulation

"""

from __future__ import division, print_function # Python 2 compatibility



from tqdm import tqdm
import numpy as np
from utils.value_iteration_and_variants import value_iteration
from utils.MDP_convertor import make_global_MDP, xyz_to_state, state_to_xyz
import time

cimport numpy as cnp

DTYPE_I = np.int
DTYPE_F = np.float64

ctypedef cnp.int_t DTYPE_Ic
ctypedef cnp.float64_t DTYPE_Fc

cimport cython
@cython.boundscheck(False)
@cython.wraparound(False)

def argmax(cnp.ndarray[DTYPE_Fc, ndim=1] Gidx):
    all_ = [0]
    max_ = Gidx[0]
    for i in range(1, Gidx.shape[0]):
        if Gidx[i] > max_:
            all_ = [i]
            max_ = Gidx[i]
        elif Gidx[i] == max_:
            all_.append(i)
    return np.random.choice(all_)

def compute_GittinsIdx(cnp.ndarray[DTYPE_Fc, ndim=2] transition, cnp.ndarray[DTYPE_Fc, ndim=1] reward, double discount_factor):
    """
    return Gittins indices using State Elimination method introduced by Isaac Sonin

    This function implements the algorithm described in section 24.3.2 of
    http://www.ece.mcgill.ca/~amahaj1/projects/bandits/book/2013-bandit-computations.pdf
    Equation numbers refer to this paper.
    """
    cdef int nb_states = reward.shape[0]
    cdef list states, Continue_set, Stop_set
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] Gidx, d, b
    states = list(range(0, nb_states))
    Gidx = np.zeros(nb_states, dtype=DTYPE_F)
    # we first find the maximum index
    cdef int alpha = np.argmax(reward)
    Gidx[alpha] = reward[alpha]
    Continue_set = [alpha]
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] Q = np.copy(transition)
    d = np.copy(reward)
    b = (1 - discount_factor) * np.ones(nb_states, dtype=DTYPE_F)
    cdef Py_ssize_t k, x
    cdef double lamb, coef
    # below is "recursion step"
    for k in range(1, nb_states):
        lamb = discount_factor / (1 - discount_factor * Q[alpha, alpha])
        Stop_set = list(np.setdiff1d(states, Continue_set))
        for x in Stop_set:
            # below is Equation (6)
            coef = lamb * Q[x, alpha]
            d[x] += coef * d[alpha]
            b[x] += coef * b[alpha]
            # below is Equation (5)
            for y in Stop_set:
                Q[x, y] += coef * Q[alpha, y]
        d[alpha] = -np.inf
        # we now compute the argmax and the corresponding index
        alpha = np.argmax(d / b)
        Gidx[alpha] = (1 - discount_factor) * d[alpha] / b[alpha]
        Continue_set.append(alpha)
    return Gidx

def make_RandomWalk(Py_ssize_t nb_states, double rew_l, double rew_r, double prob_l, double prob_r, double prob_rl):
    """
    Create a riverswim embedded with its corresponding optimal policy

    Parameters
    ----------
    nb_states : int
        Number of states in the river swim
    rew_l : float
        Reward of state 0 (leftmost state)
    rew_r : float
        Reward of final state (rightmost state)
    prob_l : float
        Probability of going left
    prob_r : float
        Probability of going right
    prob_rl : float
        Probability of going left when arriving at final state

    Returns
    -------
    prob_mat : ndarray
        Transition matrix of the chain
    rew_vec : ndarray
        Reward vector of the chain
    """
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] rew_vec = np.zeros(nb_states, dtype=DTYPE_F)
    rew_vec[0] = rew_l
    rew_vec[nb_states-1] = rew_r
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] prob_mat = np.zeros((nb_states, nb_states), dtype=DTYPE_F)
    cdef Py_ssize_t state
    cdef double pr, pl
    for state in range(nb_states):
        pr = 0.0
        if state < nb_states-1:
            prob_mat[state, state+1] = prob_r
            pr = prob_r
        pl = 0.0
        if state > 0 and state < nb_states - 1:
            prob_mat[state, state-1] = prob_l
            pl = prob_l
        if state == nb_states - 1:
            prob_mat[state, state-1] = prob_rl
            pl = prob_rl
        prob_mat[state, state] = 1.0 - pr - pl
    return prob_mat, rew_vec

def make_TaskScheduling(Py_ssize_t nb_states, Py_ssize_t N_bandits, double lamb):
    """
    Create the environment used in Q-learning for Bandit problem paper
    https://moduff.github.io/Duff_1995_q_learning_bandits.pdf
    """
    if N_bandits > 9 or nb_states > 17:
        raise ValueError("Cannot make more than 9 tasks or more than 17 states")
    cdef Py_ssize_t b, s
    cdef double rho, p
    cdef list Ps = []
    cdef list Rs = []
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] P
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] R

    for b in range(N_bandits):
        rho = 0.1*(b+1)
        P = np.zeros((nb_states, nb_states), dtype=DTYPE_F)
        R = np.zeros(nb_states, dtype=DTYPE_F)
        fs = 0.0
        for s in range(1, nb_states-1):
            p = 1.0
            for k in range(1, s):
                p *= (1-rho)*(lamb**(k-1))
            ps = (1 - ((1-rho)*(lamb**(s-1))))*p # Equation on page 19
            hazard_rate = ps / (1 - fs)
            R[s-1] = hazard_rate
            P[s-1, s] = 1 - hazard_rate
            P[s-1, nb_states-1] = hazard_rate
            fs += ps
        # the last state, s = nb_states - 2
        s = nb_states - 2
        p = 1.0
        for k in range(1, s+1):
            p *= (1-rho)*(lamb**(k-1))
        ps = (1 - (1-rho)*(lamb**s))*p
        hazard_rate = ps / (1 - fs)
        R[s] = hazard_rate
        P[s, s] = 1 - hazard_rate
        P[s, nb_states - 1] = hazard_rate
        # state nb_states-1 is absorbing state
        P[nb_states - 1, nb_states - 1] = 1.0
        Ps.append(P)
        Rs.append(R)
    return Ps, Rs

def calculate_exact_value(cnp.ndarray[DTYPE_Fc, ndim=3] true_transition, cnp.ndarray[DTYPE_Fc, ndim=2] true_reward, double discount_factor, Py_ssize_t initial_state, cnp.ndarray[Py_ssize_t, ndim=1] policy):
    """
    Calculate the value of initial_state using matrix inversion
    involving discount factor (evaluate by MDP method)

    Returns
    -------
    out : float
        Value of state initial_state when following the given policy
    """
    cdef Py_ssize_t number_of_MDP_states, N_bandits, state
    number_of_MDP_states = true_reward.shape[0]
    N_bandits = true_reward.shape[1]
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] P_opt
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] R_opt, v
    P_opt = np.zeros((number_of_MDP_states, number_of_MDP_states), dtype=DTYPE_F)
    R_opt = np.zeros(number_of_MDP_states, dtype=DTYPE_F)
    for state in range(number_of_MDP_states):
        P_opt[state, :] = true_transition[state, policy[state], :]
        R_opt[state] = true_reward[state, policy[state]]
    v = np.linalg.solve(np.eye(number_of_MDP_states, dtype=DTYPE_F) -discount_factor*P_opt, R_opt)
    return v[initial_state]

def simulate_agent(list Ps, list Rs, Py_ssize_t initial_state, cnp.ndarray[DTYPE_Ic, ndim=1] episode_lengths, object agent):
    """
    Simulate the agent over all episodes

    Returns
    -------
    out : ndarray
        episodic gaps suffered by the agent
    """
    cdef Py_ssize_t nb_states, number_of_MDP_states, N_bandits
    cdef double discount_factor = agent.discount_factor
    N_bandits = len(Rs)
    nb_states = len(Rs[0])
    number_of_MDP_states = nb_states**N_bandits

    cdef int Horizon
    cdef list gaps = []
    cdef list update_times = []
    cdef double opt_value_initial_state, value_initial_state, reward, opt_cumul_reward, cumul_reward, delta_time

    cdef cnp.ndarray[DTYPE_Ic, ndim=1] init_xyz, curr_xyz
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] init_Gidx, curr_Gidx
    init_xyz = state_to_xyz(initial_state, N_bandits, nb_states)
    init_Gidx = np.zeros(N_bandits, dtype=DTYPE_F)

    # true_transition and true_reward are needed for exact computation of value of policy
    cdef cnp.ndarray[Py_ssize_t, ndim=1] opt_policy
    cdef cnp.ndarray[DTYPE_Fc, ndim=3] true_transition
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] true_reward
    cdef bint feasible = (nb_states**N_bandits < 1025) # feasible for exact computation of value function
    if feasible:
        true_transition, true_reward = make_global_MDP(Ps, Rs)
        (opt_policy, _) = value_iteration(true_transition, true_reward, np.zeros(number_of_MDP_states, dtype=DTYPE_F), discount_factor, initial_state)
        opt_value_initial_state = calculate_exact_value(true_transition, true_reward, discount_factor, initial_state, opt_policy)
        #print(opt_value_initial_state)
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] value_refs = np.zeros((N_bandits, nb_states), dtype=DTYPE_F)
    cdef Py_ssize_t b
    for b in range(N_bandits):
        value_refs[b] = compute_GittinsIdx(Ps[b], Rs[b], discount_factor) # Gittins index of bandit b
        init_Gidx[b] = value_refs[b, init_xyz[b]] # initial Gittins index of bandit b
    #print(value_refs)
    cdef Py_ssize_t t, state, action, next_state, s, next_s

    for Horizon in tqdm(episode_lengths):#episode_lengths:#
        curr_xyz = np.copy(init_xyz)
        curr_xyz_opt = np.copy(init_xyz)
        curr_Gidx_opt = np.copy(init_Gidx)

        # track the time to update policy
        delta_time = 0.0
        delta_time += agent.update_policy()

        if feasible:
            value_initial_state = calculate_exact_value(true_transition, true_reward, discount_factor, initial_state, agent.get_policy())
            #print("policy", agent.Gittins_Idx)
            #print("value", value_initial_state)
        opt_cumul_reward = 0.0
        cumul_reward = 0.0
        for t in range(Horizon):
            # agent
            action = agent.choose_action(curr_xyz) # agent's policy
            s = curr_xyz[action]
            next_s = np.random.choice(nb_states,p=Ps[action][s, :])
            if agent.task_scheduling:
                if s != nb_states-1 and next_s == nb_states-1:  # finish the task
                    reward = 1.0
                else:
                    reward = 0.0                    # not finish the task yet or already finished
            else:
                if np.random.rand() < Rs[action][s]:
                    reward = 1.0
                else:
                    reward = 0.0

            next_xyz = np.copy(curr_xyz)
            next_xyz[action] = next_s
            cumul_reward += Rs[action][s]
            # also track the time to update observations
            delta_time += agent.update_history(curr_xyz, action, reward, next_xyz) # agent observe the {state, action random_reward, next_state}
            curr_xyz = next_xyz # state transition

            # oracle
            action_opt = argmax(curr_Gidx_opt)   # gittins policy
            s_opt = curr_xyz_opt[action_opt]
            next_s_opt = np.random.choice(nb_states,p=Ps[action_opt][s_opt, :])
            curr_xyz_opt[action_opt] = next_s_opt
            curr_Gidx_opt[action_opt] = value_refs[action_opt][next_s_opt]
            opt_cumul_reward += Rs[action_opt][s_opt]

        # otherwise, the last transition is not observed at the end of episode
        if feasible:
            gaps.append(opt_value_initial_state - value_initial_state)
        else:
            #print(opt_cumul_reward, cumul_reward)
            gaps.append(opt_cumul_reward - cumul_reward)
        update_times.append(delta_time)
    #print(agent.Gittins_Idx)
    return np.array(gaps), np.array(update_times)

def simulate_agent_ts(list Ps, list Rs, Py_ssize_t initial_state, cnp.ndarray[DTYPE_Ic, ndim=1] episode_lengths, Py_ssize_t T, object agent):
    """
    Simulate the agent over all episodes

    Returns
    -------
    out : ndarray
        the different between the cumulative reward of the oracle and the agent over time steps
    """
    cdef Py_ssize_t nb_states, number_of_MDP_states, N_bandits, timestep
    cdef double discount_factor = agent.discount_factor
    N_bandits = len(Rs)
    nb_states = len(Rs[0])
    number_of_MDP_states = nb_states**N_bandits

    cdef int Horizon, total_timesteps
    total_timesteps = np.sum(episode_lengths)
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] gap_steps = np.zeros(total_timesteps, dtype=DTYPE_F)

    cdef cnp.ndarray[DTYPE_Ic, ndim=1] init_xyz, curr_xyz
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] init_Gidx, curr_Gidx
    init_xyz = state_to_xyz(initial_state, N_bandits, nb_states)
    init_Gidx = np.zeros(N_bandits, dtype=DTYPE_F)

    cdef cnp.ndarray[DTYPE_Fc, ndim=2] value_refs = np.zeros((N_bandits, nb_states), dtype=DTYPE_F)
    cdef Py_ssize_t b
    for b in range(N_bandits):
        value_refs[b] = compute_GittinsIdx(Ps[b], Rs[b], discount_factor) # Gittins index of bandit b
        init_Gidx[b] = value_refs[b, init_xyz[b]] # initial Gittins index of bandit b
    print(np.around(value_refs, 3))
    cdef Py_ssize_t t, state, action, next_state, s, next_s
    cdef double reward
    timestep = 0

    for Horizon in tqdm(episode_lengths):
        curr_xyz = np.copy(init_xyz)
        curr_xyz_opt = np.copy(init_xyz)
        curr_Gidx_opt = np.copy(init_Gidx)
        agent.update_policy()
        for t in range(Horizon):
            # agent
            action = agent.choose_action(curr_xyz) # agent's policy
            s = curr_xyz[action]
            next_s = np.random.choice(nb_states,p=Ps[action][s, :])
            if agent.task_scheduling:
                if s != nb_states-1 and next_s == nb_states-1:  # finish the task
                    reward = 1.0
                else:
                    reward = 0.0                    # not finish the task yet or already finished
            else:
                if np.random.rand() < Rs[action][s]: # Bernoulli reward
                    reward = 1.0
                else:
                    reward = 0.0

            next_xyz = np.copy(curr_xyz)
            next_xyz[action] = next_s
            agent.update_history(curr_xyz, action, reward, next_xyz) # agent observe the {state, action random_reward, next_state}
            curr_xyz = next_xyz # state transition

            # oracle
            action_opt = argmax(curr_Gidx_opt)   # gittins policy
            s_opt = curr_xyz_opt[action_opt]
            next_s_opt = np.random.choice(nb_states,p=Ps[action_opt][s_opt, :])
            curr_xyz_opt[action_opt] = next_s_opt
            curr_Gidx_opt[action_opt] = value_refs[action_opt][next_s_opt]

            gap_steps[timestep] = Rs[action_opt][s_opt] - Rs[action][s]
            timestep += 1

        # otherwise, the last transition is not observed at the end of episode
    return np.array(gap_steps[:T])

def compute_regret(object comm, int nb_procs, int proc_id, int nb_sims, int nb_episodes, list Ps, list Rs, object agent):
    """
    This function handles the random seeds for each simulation and gathers the result

    Each time this function is called, the random seed is reset.
    Than, a vector of seeds is drawn and each processor has its own seed.
    This allows each agent to have the same sequence of horizons.
    """
    np.random.seed(123)
    cdef double discount_factor = agent.discount_factor
    cdef cnp.ndarray[DTYPE_Ic, ndim=2] seeds
    if proc_id == 0: # only processor root draws the seed
        seeds = np.random.choice(1000, size=(nb_procs,nb_sims), replace=False)
    else:
        seeds = np.empty((nb_procs, nb_sims), dtype=DTYPE_I)

    cdef cnp.ndarray[DTYPE_Ic, ndim=1] recv_seeds
    recv_seeds = np.empty(nb_sims, dtype=DTYPE_I)
    comm.Scatter(seeds, recv_seeds, root=0) # distribute the seeds drawn
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] gaps = np.empty((nb_sims, nb_episodes), dtype=DTYPE_F)
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] update_times = np.empty((nb_sims, nb_episodes), dtype=DTYPE_F)
    cdef cnp.ndarray[DTYPE_Ic, ndim=1] episode_lengths
    cdef Py_ssize_t i

    for i in range(nb_sims):
        np.random.seed(recv_seeds[i])
        episode_lengths = np.random.geometric(p=1.0-discount_factor, size=nb_episodes) # generate a sequence of horizons
        gaps[i, :], update_times[i, :] = simulate_agent(Ps, Rs, 0, episode_lengths, agent)
        agent.reset()

    cdef cnp.ndarray[DTYPE_Fc, ndim=3] recv_gaps = np.empty([nb_procs, nb_sims, nb_episodes], dtype=DTYPE_F)
    cdef cnp.ndarray[DTYPE_Fc, ndim=3] recv_update_times = np.empty([nb_procs, nb_sims, nb_episodes], dtype=DTYPE_F)
    comm.Gather(gaps, recv_gaps, root=0)    # gather the regret from each processor
    comm.Gather(update_times, recv_update_times, root=0)    # gather the regret from each processor
    if proc_id == 0:
        return recv_gaps.reshape((nb_sims*nb_procs, nb_episodes)), recv_update_times.reshape((nb_sims*nb_procs, nb_episodes))

def compute_bayesian_regret(object comm, int nb_procs, int proc_id, int nb_sims, int nb_episodes, int nb_mdps_per_proc, object agent):
    """
    This function handles the random seeds for each simulation and gathers the result

    Each time this function is called, the random seed is reset.
    Than, a vector of seeds is drawn and scatter to each processor.
    This allows each agent to have the same mdp sample and sequence of horizons
    """
    np.random.seed(123)
    cdef double discount_factor = agent.discount_factor
    cdef cnp.ndarray[DTYPE_Ic, ndim=1] seeds
    if proc_id == 0: # only processor root draws the seed
        seeds = np.random.choice(1000, size=nb_procs, replace=False)
    else:
        seeds = np.empty(nb_procs, dtype=DTYPE_I)

    cdef cnp.ndarray[DTYPE_Ic, ndim=1] recv_seed = np.empty(1, dtype=DTYPE_I)
    comm.Scatter(seeds, recv_seed, root=0) # distribute the seeds drawn
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] gaps
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] update_times
    cdef cnp.ndarray[DTYPE_Ic, ndim=1] episode_lengths
    cdef Py_ssize_t i, j, N_bandits, nb_states, total_nb_sims, idx
    cdef init_state = 0
    N_bandits = 3
    nb_states = 4
    total_nb_sims = nb_sims*nb_mdps_per_proc
    cdef list Ps, Rs
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] R_1, R_2, R_3
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] P_1, P_2, P_3
    cdef double pL, pR, pRL, rL, rR
    gaps = np.empty((total_nb_sims, nb_episodes), dtype=DTYPE_F)
    update_times = np.empty((total_nb_sims, nb_episodes), dtype=DTYPE_F)

    np.random.seed(recv_seed[0])    # so that all agents play with the same MDPs and sequence of horizons

    idx = 0
    for i in range(nb_mdps_per_proc):
        pRL = np.random.rand()          # draw transition probability
        pL, pR, _ = np.random.dirichlet(np.ones(3, dtype=DTYPE_I)) # pL, pR must live in a 2-simplex
        rL, rR = np.random.rand(2)      # draw mean reward
        P_1, R_1 = make_RandomWalk(nb_states, rL, rR, pL, pR, pRL)

        pRL = np.random.rand()          # draw transition probability
        pL, pR, _ = np.random.dirichlet(np.ones(3, dtype=DTYPE_I)) # pL, pR must live in a 2-simplex
        rL, rR = np.random.rand(2)      # draw mean reward
        P_2, R_2 = make_RandomWalk(nb_states, rL, rR, pL, pR, pRL)

        pRL = np.random.rand()          # draw transition probability
        pL, pR, _ = np.random.dirichlet(np.ones(3, dtype=DTYPE_I)) # pL, pR must live in a 2-simplex
        rL, rR = np.random.rand(2)      # draw mean reward
        P_3, R_3 = make_RandomWalk(nb_states, rL, rR, pL, pR, pRL)

        Ps = [P_1, P_2, P_3]
        Rs = [R_1, R_2, R_3]

        for j in range(nb_sims):
            episode_lengths = np.random.geometric(p=1.0-discount_factor, size=nb_episodes) # generate a sequence of horizons
            gaps[idx, :], update_times[idx, :] = simulate_agent(Ps, Rs, init_state, episode_lengths, agent)
            agent.reset()
            idx += 1

    cdef cnp.ndarray[DTYPE_Fc, ndim=3] recv_gaps = np.empty([nb_procs, total_nb_sims, nb_episodes], dtype=DTYPE_F)
    cdef cnp.ndarray[DTYPE_Fc, ndim=3] recv_update_times = np.empty([nb_procs, total_nb_sims, nb_episodes], dtype=DTYPE_F)
    comm.Gather(gaps, recv_gaps, root=0)    # gather the regret from each processor
    comm.Gather(update_times, recv_update_times, root=0)    # gather the regret from each processor
    if proc_id == 0:
        return recv_gaps.reshape((nb_procs*total_nb_sims, nb_episodes)), recv_update_times.reshape((nb_procs*total_nb_sims, nb_episodes))

def compute_regret_ts(object comm, int nb_procs, int proc_id, int nb_sims, int nb_episodes, list Ps, list Rs, object agent):
    """
    This function handles the random seeds for each simulation and gathers the result

    Each time this function is called, the random seed is reset.
    Than, a vector of seeds is drawn and each processor has its own seed.
    This allows each agent to have the same sequence of horizons.
    """
    np.random.seed(123)
    cdef double discount_factor = agent.discount_factor
    cdef cnp.ndarray[DTYPE_Ic, ndim=2] seeds
    if proc_id == 0: # only processor root draws the seed
        seeds = np.random.choice(1000, size=(nb_procs,nb_sims), replace=False)
    else:
        seeds = np.empty((nb_procs, nb_sims), dtype=DTYPE_I)

    cdef cnp.ndarray[DTYPE_Ic, ndim=1] recv_seeds
    recv_seeds = np.empty(nb_sims, dtype=DTYPE_I)
    comm.Scatter(seeds, recv_seeds, root=0) # distribute the seeds drawn
    cdef cnp.ndarray[DTYPE_Ic, ndim=2] episode_lengths = np.zeros((nb_sims, nb_episodes), dtype=DTYPE_I)
    cdef cnp.ndarray[Py_ssize_t, ndim=1]  total_timesteps = np.zeros(nb_sims, dtype=DTYPE_I)

    cdef Py_ssize_t i
    for i in range(nb_sims):
        np.random.seed(recv_seeds[i])
        episode_lengths[i, :] = np.random.geometric(p=1.0-discount_factor, size=nb_episodes) # generate a sequence of horizons
        total_timesteps[i] = np.sum(episode_lengths[i, :])

    cdef cnp.ndarray[Py_ssize_t, ndim=2] recv_total_timesteps = np.empty([nb_procs, nb_sims], dtype=DTYPE_I)
    comm.Gather(total_timesteps, recv_total_timesteps, root=0)
    cdef Py_ssize_t T
    if proc_id == 0:
        T = np.min(recv_total_timesteps)
    T = comm.bcast(T, root=0)

    cdef cnp.ndarray[DTYPE_Fc, ndim=2] gaps = np.empty((nb_sims, T), dtype=DTYPE_F)
    for i in range(nb_sims):
        gaps[i, :] = simulate_agent_ts(Ps, Rs, 0, episode_lengths[i], T, agent)
        agent.reset()

    cdef cnp.ndarray[DTYPE_Fc, ndim=3] recv_gaps = np.empty([nb_procs, nb_sims, T], dtype=DTYPE_F)
    comm.Gather(gaps, recv_gaps, root=0)    # gather the regret from each processor
    if proc_id == 0:
        return recv_gaps.reshape((nb_sims*nb_procs, T))
