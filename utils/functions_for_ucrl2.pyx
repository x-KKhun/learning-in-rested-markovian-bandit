# -*- coding: utf-8 -*-

""" Vanilla UCRL2 algorithm:

- new_episode: compute estimators from observations collected so far and call extended_value_iteration
- extended_value_iteration: perform the algorithm of Section 3.1.2 of https://www.jmlr.org/papers/volume11/jaksch10a/jaksch10a.pdf
- max_proba: perform the algorithm of Figure 2 of https://www.jmlr.org/papers/volume11/jaksch10a/jaksch10a.pdf

"""

from __future__ import division, print_function # Python 2 compatibility



cimport numpy as cnp
import numpy as np

DTYPE_I = np.int
DTYPE_F = np.float64

ctypedef cnp.int_t DTYPE_Ic
ctypedef cnp.float64_t DTYPE_Fc

cimport cython
@cython.boundscheck(False)
@cython.wraparound(False)
def new_episode(cnp.ndarray[DTYPE_Fc, ndim=2] Rk, cnp.ndarray[DTYPE_Fc, ndim=3] Pk, cnp.ndarray[DTYPE_Fc, ndim=2] Nk, cnp.ndarray[DTYPE_Fc, ndim=1] value_func, double t, double discount_factor, double delta=0.01):
    """
    Re-estimate the MDP according to the history

    Parameters
    ----------
    Rk : ndarray
        The cumulative reward of each state-action pair
    Pk : ndarray
        The transition counters of each state-action pair
    Nk : ndarray
        The state-action pair counters
    t : float
        The timesteps that MDP is played so far
    discount_factor : float
        The discount factor in the MDP
    delta : float
        The critical level of the confidence interval (1-(confidence level/100))

    Returns
    -------
    policy : ndarray
        The best policy according the history
    u1 : ndarray
        The value of the best policy in the best possible MDP
    """
    cdef Py_ssize_t number_of_states, number_of_actions, s, a
    number_of_states = Rk.shape[0]
    number_of_actions = Rk.shape[1]
    # distances
    # empirical estimators
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] r_distance, r_estimate, p_distance
    cdef cnp.ndarray[DTYPE_Fc, ndim=3] p_estimate
    r_distance = np.zeros_like(Rk, dtype=DTYPE_F)
    p_distance = np.zeros_like(Rk, dtype=DTYPE_F)
    r_estimate = np.zeros_like(Rk, dtype=DTYPE_F)
    p_estimate = np.zeros_like(Pk, dtype=DTYPE_F)
    cdef double div
    cdef double ucb
    for s in range(number_of_states):
        for a in range(number_of_actions):
            div = max(1.0, Nk[s, a])
            r_estimate[s, a] = Rk[s, a] / div
            p_estimate[s, a, :] = Pk[s, a, :] / div
            ucb = np.log(2 * number_of_states * number_of_actions * t / delta)
            r_distance[s, a] = np.sqrt(ucb / (2*div))
            p_distance[s, a] = np.sqrt(ucb / div)
    if discount_factor == 1.0:
        return undiscount_extended_value_iteration(r_estimate, p_estimate, r_distance, p_distance, value_func)
    else:
        return extended_value_iteration(r_estimate, p_estimate, r_distance, p_distance, value_func, discount_factor)

def undiscount_extended_value_iteration(cnp.ndarray[DTYPE_Fc, ndim=2] r_estimate, cnp.ndarray[DTYPE_Fc, ndim=3] p_estimate, cnp.ndarray[DTYPE_Fc, ndim=2] r_distance, cnp.ndarray[DTYPE_Fc, ndim=2] p_distance, cnp.ndarray[DTYPE_Fc, ndim=1] value_func, double epsilon=0.01):
    """
    Calculate the best policy by maximizing over the plausible MDPs

    Parameters
    ----------
    r_estimate : ndarray
        The reward estimator of each state-action pair
    p_estimate : ndarray
        The probability estimator vector of each state-action pair
    r_distance : ndarray
        The half width of the confidence interval over the reward estimator
    p_distance : ndarray
        The half width of the confidence interval over the probability estimator
    epsilon : float
        The threshold smaller than which the value function is considered as converged

    Returns
    -------
    policy : ndarray
        The best policy from the maximization
    u1 : ndarray
        The value of the best policy in the best possible MDP among the plausible MDPs
    """
    cdef Py_ssize_t number_of_states, number_of_actions, s, a
    number_of_states = r_estimate.shape[0]
    number_of_actions = r_estimate.shape[1]

    cdef cnp.ndarray[Py_ssize_t, ndim=1] sorted_indices = np.arange(number_of_states)
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] u0, u1, diff, max_p_sa
    u0 = np.copy(value_func)
    u1 = np.zeros_like(u0, dtype=DTYPE_F)
    cdef cnp.ndarray[Py_ssize_t, ndim=1] policy = np.zeros(number_of_states, dtype=DTYPE_I)

    cdef double temp
    while True:
        for s in range(number_of_states):
            for a in range(number_of_actions):
                max_p_sa = max_proba(p_estimate[s, a], p_distance[s, a], sorted_indices)
                temp = r_estimate[s, a] + r_distance[s, a] + max_p_sa.dot(u0)
                if (a == 0) or (temp > u1[s]):
                    u1[s] = temp
                    policy[s] = a
        diff = u1 - u0
        if (np.max(diff) - np.min(diff)) < epsilon:
            # stop the algorithm when the span of value function is small
            break
        else:
            u0 = np.copy(u1)
            u1 = np.zeros_like(u0)
            sorted_indices = np.argsort(u0)
    return (policy, u1)

def extended_value_iteration(cnp.ndarray[DTYPE_Fc, ndim=2] r_estimate, cnp.ndarray[DTYPE_Fc, ndim=3] p_estimate, cnp.ndarray[DTYPE_Fc, ndim=2] r_distance, cnp.ndarray[DTYPE_Fc, ndim=2] p_distance, cnp.ndarray[DTYPE_Fc, ndim=1] value_func, double discount_factor, double epsilon=0.01):
    """
    Calculate the best policy by maximizing over the plausible MDPs

    Parameters
    ----------
    r_estimate : ndarray
        The reward estimator of each state-action pair
    p_estimate : ndarray
        The probability estimator vector of each state-action pair
    r_distance : ndarray
        The half width of the confidence interval over the reward estimator
    p_distance : ndarray
        The half width of the confidence interval over the probability estimator
    discount_factor : float
        The discount factor in the MDP
    epsilon : float
        The threshold smaller than which the value function is considered as converged

    Returns
    -------
    policy : ndarray
        The best policy from the maximization
    u1 : ndarray
        The value of the best policy in the best possible MDP among the plausible MDPs
    """
    cdef Py_ssize_t number_of_states, number_of_actions, s, a
    number_of_states = r_estimate.shape[0]
    number_of_actions = r_estimate.shape[1]

    cdef cnp.ndarray[Py_ssize_t, ndim=1] sorted_indices = np.arange(number_of_states)
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] u0, u1, diff, max_p_sa
    u0 = np.copy(value_func)
    u1 = np.zeros_like(u0, dtype=DTYPE_F)
    cdef cnp.ndarray[Py_ssize_t, ndim=1] policy = np.zeros(number_of_states, dtype=DTYPE_I)

    cdef double temp
    while True:
        for s in range(number_of_states):
            for a in range(number_of_actions):
                max_p_sa = max_proba(p_estimate[s, a], p_distance[s, a], sorted_indices)
                temp = r_estimate[s, a] + r_distance[s, a] + discount_factor * max_p_sa.dot(u0)
                if temp > u1[s]:
                    u1[s] = temp
                    policy[s] = a
        diff = u1 - u0
        if abs(diff).sum() < epsilon:
            # stop the algorithm when the span of value function is small
            break
        else:
            u0 = np.copy(u1)
            u1 = np.zeros_like(u0)
            sorted_indices = np.argsort(u0)
    return (policy, u1)

def max_proba(cnp.ndarray[DTYPE_Fc, ndim=1] p_estimate_sa, double p_distance_sa, cnp.ndarray[Py_ssize_t, ndim=1] sorted_indices):
    """
    Adjust the probability so that max_p_sa.dot(v) is maximised. The algo. is introduced by Strehl and Littman (2008)

    Parameters
    ----------
    p_estimate_sa : ndarray
        The estimated probability vector of a state-action pair
    p_distance_sa : ndarray
        The half width of confidence interval over the estimated probability
    sorted_indices : ndarray
        The array of indexes where sorted_indices[0] is the index of the smallest value of current value function

    Returns
    -------
    out : ndarray
        The maximised probability (a N-array)
    """
    cdef Py_ssize_t number_of_states = p_estimate_sa.shape[0]
    cdef double min1 = min([1.0, p_estimate_sa[sorted_indices[-1]] + (p_distance_sa / 2)])
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] max_p = np.zeros(number_of_states)
    cdef Py_ssize_t l
    if min1 == 1:
        max_p[sorted_indices[-1]] = 1
    else:
        max_p = np.copy(p_estimate_sa)
        max_p[sorted_indices[-1]] += p_distance_sa / 2
        l = 0
        while sum(max_p) > 1:
            max_p[sorted_indices[l]] = max([0, 1 - sum(max_p) + max_p[sorted_indices[l]]])
            #max_p[l] = max([0, 1 - sum(max_p) + max_p[l]])
            l += 1
    return max_p
