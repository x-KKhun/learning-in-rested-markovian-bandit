# -*- coding: utf-8 -*-

""" Value Iteration (VI) algorithm

- one_state_at_a_time_value_iteration: compute the optimal value and optimal action for a given state
- asynchrone_all_states_at_a_time_value_iteration: one iter. of VI: for each state, call one_state_at_a_time_value_iteration
- synchrone_all_states_at_a_time_value_iteration: one iter. of VI: update optimal value of all states at the same time
- value_iteration: perform algorithm: either asynchrone_all_states_at_a_time_value_iteration or synchrone_all_states_at_a_time_value_iteration

"""

from __future__ import division, print_function # Python 2 compatibility




cimport numpy as cnp
import numpy as np

DTYPE_I = np.int
DTYPE_F = np.float64

ctypedef cnp.int_t DTYPE_Ic
ctypedef cnp.float64_t DTYPE_Fc

cimport cython
@cython.boundscheck(False)
@cython.wraparound(False)
def one_state_at_a_time_value_iteration(cnp.ndarray[DTYPE_Fc, ndim=1] V, cnp.ndarray[DTYPE_Fc, ndim=2] transition_matrix, cnp.ndarray[DTYPE_Fc, ndim=1] reward, double discount_factor):
    """
    Perform Optimal Bellman's Operator

    Parameters
    ----------
    V : ndarray
        The current value function (N-vector)
    transition_matrix : ndarray
        The transition probability matrix of the current state (AxN-matrix)
    reward : ndarray
        The reward vector of each action for the current state (A vector)
    discount_factor : float
        The discount factor

    Returns
    -------
    Opt_a : int
        optimal action for the current state
    out : float
        optimal value of the current state
    """
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] Q = reward + discount_factor*transition_matrix.dot(V)
    cdef DTYPE_Ic Opt_a = np.argmax(Q) # optimal action
    return (Opt_a, Q[Opt_a])

def asynchrone_all_states_at_a_time_value_iteration(cnp.ndarray[DTYPE_Fc, ndim=1] V, cnp.ndarray[DTYPE_Fc, ndim=3] transition_matrix, cnp.ndarray[DTYPE_Fc, ndim=2] reward, double discount_factor):
    cdef Py_ssize_t number_of_states = reward.shape[0]
    cdef cnp.ndarray[DTYPE_Ic, ndim=1] policy = np.zeros(number_of_states, dtype=DTYPE_I)
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] V_tmp = np.copy(V)
    cdef Py_ssize_t s
    for s in range(number_of_states):
        (policy[s], V_tmp[s]) = one_state_at_a_time_value_iteration(V_tmp, transition_matrix[s], reward[s], discount_factor)
    return (policy, V_tmp)

def synchrone_all_states_at_a_time_value_iteration(cnp.ndarray[DTYPE_Fc, ndim=1] V, cnp.ndarray[DTYPE_Fc, ndim=3] transition_matrix, cnp.ndarray[DTYPE_Fc, ndim=2] reward, double discount_factor):
    cdef cnp.ndarray[DTYPE_Fc, ndim=2] Q = reward + discount_factor*transition_matrix.dot(V)
    return (np.argmax(Q, axis=1), np.max(Q, axis=1))

def undiscount_value_iteration(cnp.ndarray[DTYPE_Fc, ndim=3] transition_matrix, cnp.ndarray[DTYPE_Fc, ndim=2] reward, cnp.ndarray[DTYPE_Fc, ndim=1] value_func, int initial_state=-1, double epsilon=0.01, bint asynchrone=False):
    """
    applies Value Iteration to compute the value and the optimal stationary policy

    Parameters
    ----------
    transition_matrix : NxAxN tensor
    reward : NxA matrix

    Returns
    -------
    policy : ndarray
        the optimal stationary policy (a N-vector)
    V_tmp : ndarray
        The value function when following the optimal stationary policy
    """
    cdef Py_ssize_t number_of_states, number_of_actions
    number_of_states = transition_matrix.shape[0]
    number_of_actions = transition_matrix.shape[1]
    cdef cnp.ndarray[DTYPE_Ic, ndim=1] policy = np.zeros(number_of_states, dtype=DTYPE_I) # policy
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] V = np.copy(value_func) # value function
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] V_tmp = np.zeros_like(V, dtype=DTYPE_F)
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] diff
    while True:
        if asynchrone:
            policy, V_tmp = asynchrone_all_states_at_a_time_value_iteration(V, transition_matrix, reward, 1.0)
        else:
            policy, V_tmp = synchrone_all_states_at_a_time_value_iteration(V, transition_matrix, reward, 1.0)
        diff = V_tmp - V
        if max(diff) - min(diff) < epsilon:
            break
        else:
            V = np.copy(V_tmp)
            V_tmp = np.zeros_like(V, dtype=DTYPE_F)
    #print(V[number_of_states-1], V_tmp[number_of_states-1])
    if initial_state == -1:
        return (policy, V_tmp)
    else:
        return (policy, V_tmp[initial_state])

def value_iteration(cnp.ndarray[DTYPE_Fc, ndim=3] transition_matrix, cnp.ndarray[DTYPE_Fc, ndim=2] reward, cnp.ndarray[DTYPE_Fc, ndim=1] value_func, double discount_factor, int initial_state=-1, double epsilon=0.01, bint asynchrone=False):
    """
    applies Value Iteration to compute the value and the optimal stationary policy

    Parameters
    ----------
    transition_matrix : NxAxN tensor
    reward : NxA matrix
    discount_factor : discount factor

    Returns
    -------
    policy : ndarray
        the optimal stationary policy (a N-vector)
    V_tmp : ndarray
        The value function when following the optimal stationary policy
    """
    cdef Py_ssize_t number_of_states, number_of_actions
    number_of_states = transition_matrix.shape[0]
    number_of_actions = transition_matrix.shape[1]
    cdef cnp.ndarray[DTYPE_Ic, ndim=1] policy = np.zeros(number_of_states, dtype=DTYPE_I) # policy
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] V = np.copy(value_func) # value function
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] V_tmp = np.zeros_like(V, dtype=DTYPE_F)
    cdef cnp.ndarray[DTYPE_Fc, ndim=1] diff
    while True:
        if asynchrone:
            policy, V_tmp = asynchrone_all_states_at_a_time_value_iteration(V, transition_matrix, reward, discount_factor)
        else:
            policy, V_tmp = synchrone_all_states_at_a_time_value_iteration(V, transition_matrix, reward, discount_factor)
        diff = V_tmp - V
        if abs(diff).sum() < epsilon:
            break
        else:
            V = np.copy(V_tmp)
            V_tmp = np.zeros_like(V, dtype=DTYPE_F)
    #print(V[number_of_states-1], V_tmp[number_of_states-1])
    if initial_state == -1:
        return (policy, V_tmp)
    else:
        return (policy, V_tmp[initial_state])
