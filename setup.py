# -*- coding: utf-8 -*-
__author__ = "Kimang Khun"
__email__ = "khun.kimang@gmail.com"

""" script for compiling Cython

"""

from __future__ import division, print_function # Python 2 compatibility



from setuptools import setup, Extension
from Cython.Build import cythonize
import numpy

extensions = [
    Extension("MDP_convertor", ["./utils/MDP_convertor.pyx"], include_dirs=[numpy.get_include()]),
    Extension("experiments", ["./utils/experiments.pyx"], include_dirs=[numpy.get_include()]),
    Extension("functions_for_ucrl2", ["./utils/functions_for_ucrl2.pyx"], include_dirs=[numpy.get_include()]),
    Extension("mdp_transition_dirichlet_sampling", ["./utils/mdp_transition_dirichlet_sampling.pyx"], include_dirs=[numpy.get_include()]),
    Extension("mdp_reward_beta_sampling", ["./utils/mdp_reward_beta_sampling.pyx"], include_dirs=[numpy.get_include()]),
    Extension("mdp_reward_normalGamma_sampling", ["./utils/mdp_reward_normalGamma_sampling.pyx"], include_dirs=[numpy.get_include()]),
    Extension("value_iteration_and_variants", ["./utils/value_iteration_and_variants.pyx"], include_dirs=[numpy.get_include()])
]

setup(
    ext_modules=cythonize(extensions),
    zip_safe=True
)
