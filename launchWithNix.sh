#!/usr/bin/env bash

# Check if Nix is installed
if ! command -v nix &> /dev/null
then
  echo "You need to install Nix: https://nixos.org"
  echo "If you have just installed, you might miss some installation instructions."
  echo "If Nix is properly installed, you should be able to use terminal command 'nix'."
  exit
fi

# Parameters of the experiment
DISCOUNT=0.99 # discount factor
EPISODES=3000 # number of episodes in each simulation
NB_PROCS=8 # number of processors used in experiment (bounded by the maximum number of cores in your local machine)

echo "### Launching simulations for Scenario 1"
echo "**Warning**: This may take four days!"
SCENE=0
NB_SIMS=$((80/$NB_PROCS)) # number of simulations to be done by each processor
nix-shell --pure shell.nix --run "python setup.py build_ext --build-lib './utils' && mpiexec -n $NB_PROCS python mpi_regret.py $DISCOUNT $EPISODES $NB_SIMS $SCENE"

echo "### Launching simulations for Scenario 2: larger example"
SCENE=1
NB_SIMS=$((240/$NB_PROCS)) # number of simulations to be done by each processor
nix-shell --pure shell.nix --run "python setup.py build_ext --build-lib './utils' && mpiexec -n $NB_PROCS python mpi_regret.py $DISCOUNT $EPISODES $NB_SIMS $SCENE"

echo "### Launching simulations for Scenario 3: different priors"
SCENE=2
NB_MDPS_PER_PROC=$((16/$NB_PROCS)) # number of mdps drawn by each processor
NB_SIMS_PER_MDP=5 # number of simulations done for each MDP drawn
nix-shell --pure shell.nix --run "python setup.py build_ext --build-lib './utils' && mpiexec -n $NB_PROCS python mpi_regret.py $DISCOUNT $EPISODES $NB_SIMS_PER_MDP $SCENE $NB_MDPS_PER_PROC"
