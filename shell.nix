{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/20.03.tar.gz") {} }:

let
  self = rec {
    inherit pkgs;
    pythonPackages = pkgs.python3Packages;

    dev-shell = pkgs.mkShell {
      name = "dev-shell";
      buildInputs = with pythonPackages; [
        ipython
      ] ++ [
        numpy
        matplotlib
        seaborn
        cython
        mpi4py
        h5py
        setuptools
        tqdm
        jupyter
        ipython
        pkgs.openmpi
        pkgs.openssh
      ];
    };
  };
in
  self
