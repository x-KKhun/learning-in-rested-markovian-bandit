# -*- coding: utf-8 -*-
""" MB-PSRL algorithm """

from __future__ import division, print_function # Python 2 compatibility



from learners.markovian_bandit_learner import *
from utils.experiments import compute_GittinsIdx

class MB_PSRL(PSRL_Learner):
    def reset(self):
        super().reset()
        self.Gittins_Idx = np.zeros((self.N_bandits, self.nb_states), dtype=np.float64)

    def update_policy(self):
        """
        Calculate the Gittins Index for each state of each bandit:
        - For each bandit:
            - draw the reward and the transition from posterior
            - compute Gittins index the drawn arm
        """
        start_update = timeit.default_timer()   # start update
        for b in range(self.N_bandits):
            sampled_transition = draw_transition_matrices(self.posterior_T[b])
            sampled_reward = post_update[self.pu_idx].draw_reward_matrix(self.posterior_R[b])
            self.Gittins_Idx[b] = compute_GittinsIdx(np.squeeze(sampled_transition), np.squeeze(sampled_reward), self.discount_factor)
        stop_update = timeit.default_timer()
        return stop_update - start_update


    def choose_action(self, xyz):
        """
        Convert Global MDP state into Markvian bandit state and choose bandit according to the index

        Parameters
        ----------
        state : int
            The current state of global MDP

        Returns
        -------
        out : int
            The action for the current state
        """
        Gidx = [self.Gittins_Idx[b, xyz[b]] for b in range(self.N_bandits)]
        return np.argmax(Gidx)

    def name(self):
        return "MB-PSRL"
