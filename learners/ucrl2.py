# -*- coding: utf-8 -*-
__author__ = "Kimang Khun"
__email__ = "khun.kimang@gmail.com"

""" Vanilla UCRL2 algorithm

Algorithm from https://www.jmlr.org/papers/volume11/jaksch10a/jaksch10a.pdf
However, we adapt some constants to our problem.
"""

from __future__ import division, print_function # Python 2 compatibility



from learners.markovian_bandit_learner import *

class UCRL2(UCRL2_Learner):
    def __init__(self, N_bandits, nb_states, discount_factor, delta, task_scheduling, constraint=True):
        self.delta = delta
        self.constraint = constraint
        self.MDP_Nb_states = int(np.power(nb_states, N_bandits))
        super().__init__(N_bandits, nb_states, discount_factor, task_scheduling)

    def reset(self):
        self.value_func = np.zeros(self.MDP_Nb_states, dtype=np.float64)
        self.policy = np.zeros(self.MDP_Nb_states, dtype=np.int)
        if self.constraint: # for MB-UCRL2
            self.possible_transitions = []
            for s in range(self.MDP_Nb_states):
                xyz = state_to_xyz(s, self.N_bandits, self.nb_states)
                self.possible_transitions.append([])
                for a in range(self.N_bandits):
                    self.possible_transitions[s].append([])
                    xyz_prime = np.copy(xyz)
                    for x in range(self.nb_states):
                        xyz_prime[a] = x
                        sprime = xyz_to_state(xyz_prime, self.N_bandits, self.nb_states)
                        self.possible_transitions[s][a].append(sprime)
        super().reset()


    def update_policy(self):
        """
        In original algorithm, the update is done in choose_action(self, state) using doubling trick

        Create counters of global MDP from counters of all bandits
        Use the created counters to calculate the optimal policy and its value function for global MDP
        """
        start_update = 0.0
        if self.constraint: # for MB-UCRL2
            start_update = timeit.default_timer()   # start update
            self.new_episode(epsilon=0.01)
        else:
            start_update = timeit.default_timer()   # start update
            Pk, Rk = make_global_MDP(self.Pk, self.Rk)
            Nk = np.sum(Pk, axis=2)
            t = np.sum(Nk) + 1.0
            self.policy, self.value_func = new_episode(Rk, Pk, Nk, self.value_func, t, self.discount_factor)
        stop_update = timeit.default_timer()        # stop update
        return stop_update - start_update

    def new_episode(self, epsilon=0.01): # for MB-UCRL2
        self.r_estimate = np.zeros((self.MDP_Nb_states, self.N_bandits), dtype=np.float64) # reward vector
        self.p_estimate = np.zeros((self.MDP_Nb_states, self.N_bandits, self.MDP_Nb_states), dtype=np.float64) # transition matrix
        self.r_distance = np.zeros_like(self.r_estimate, dtype=np.float64)
        self.p_distance = np.zeros_like(self.r_estimate, dtype=np.float64)
        t = np.sum(self.Pk) + 1.0

        for s in range(self.MDP_Nb_states):
            xyz = state_to_xyz(s, self.N_bandits, self.nb_states)
            for a in range(self.N_bandits):
                div = max(1, np.sum(self.Pk[a][xyz[a]]))
                bonus = 3 * self.nb_states * self.N_bandits * t / self.delta
                self.r_estimate[s, a] = self.Rk[a][xyz[a]] / div
                self.r_distance[s, a] = np.sqrt(np.log(2*bonus) / (2*div))
                self.p_distance[s, a] = np.sqrt(2 * np.log((2**self.nb_states) * bonus) / div)

                for x in range(self.nb_states):
                    self.p_estimate[s, a, self.possible_transitions[s][a][x]] = self.Pk[a][xyz[a], x] / div

        u0 = np.copy(self.value_func)
        u1 = np.zeros_like(u0, dtype=np.float64)
        sorted_indices = np.argsort(u0)
        self.policy = np.zeros(self.MDP_Nb_states, dtype=np.int)

        while True:
            for s in range(self.MDP_Nb_states):
                for a in range(self.N_bandits):
                    max_p_sa = self.max_proba(s, a, sorted_indices)
                    temp = self.r_estimate[s, a] + self.r_distance[s, a] + self.discount_factor * max_p_sa.dot(u0)
                    if temp > u1[s]:
                        u1[s] = temp
                        self.policy[s] = a
            diff = u1 - u0
            if abs(diff).sum() < epsilon:
                # stop the algorithm when the span of value function is small
                break
            else:
                u0 = np.copy(u1)
                u1 = np.zeros_like(u0, dtype=np.float64)
                sorted_indices = np.argsort(u0)
        self.value_func = u1

    def max_proba(self, s, a, sorted_indices): # for MB-UCRL2
        l = self.MDP_Nb_states - 1
        while not(sorted_indices[l] in self.possible_transitions[s][a]):
            l -= 1
        sprime = sorted_indices[l]
        p_distance_sa = self.p_distance[s, a]
        min1 = min([1.0, self.p_estimate[s, a, sprime] + (p_distance_sa / 2)])
        if min1 == 1:
            max_p = np.zeros(self.MDP_Nb_states)
            max_p[sprime] = 1.0
        else:
            max_p = np.copy(self.p_estimate[s, a])
            max_p[sprime] += p_distance_sa / 2
            l = 0
            while sum(max_p) > 1:
                sprime = self.possible_transitions[s][a][l] # respect the constraint of transition
                max_p[sprime] = max([0, 1 - sum(max_p) + max_p[sprime]])
                #max_p[l] = max([0, 1 - sum(max_p) + max_p[l]])
                l += 1
        return max_p

    def choose_action(self, xyz):
        state = xyz_to_state(xyz, self.N_bandits, self.nb_states)
        return self.policy[state]

    def name(self):
        if self.constraint:
            return "MB-UCRL2"
        else:
            return "UCRL2"

    def get_policy(self):
        return self.policy
